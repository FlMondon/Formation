import pytest
from django.urls import resolve, reverse
from .views import quizz
from pytest_django.asserts import assertTemplateUsed
# Create your tests here.

@pytest.mark.django_db
def test_quizz_template(client):
    url = reverse('quizz')
    response = client.get(url)
    assert response.status_code == 200
    assertTemplateUsed(response,"quizz/quizz.html")




def test_quizz_view(): # new
    view = resolve('/quizz/')
    assert view.func.__name__==quizz.__name__