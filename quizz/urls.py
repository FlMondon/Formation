from django.urls import path
from .views import quizz

urlpatterns = [
    path("", quizz, name="quizz" ),
    path("quizz_results/", quizz, name="quizz_results")
    ]