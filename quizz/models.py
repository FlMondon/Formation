from django.db import models
from django.contrib.auth import get_user_model
import uuid 

class scoreQuizz(models.Model):

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True)
    score = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.date