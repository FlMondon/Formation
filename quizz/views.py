from django.shortcuts import render
import requests
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from .models import scoreQuizz



@login_required
def quizz(request):
    if request.method == 'POST':
        questions = request.session.get('questions', [])
        score = 0
        for question in questions:
                user_answer = request.POST.get(str(question['id']), '').strip()
                
                if user_answer == question['correct_answer']:
                    
                    score += 1
        result = scoreQuizz(user=request.user, score=score)
        result.save()
        quiz_results = scoreQuizz.objects.filter(user=request.user).order_by('-date')[0]
        best_results = scoreQuizz.objects.all().order_by('-score')[:3]
        print(best_results)
        request.session['questions'] = None
        return render(request, "quizz/quizz_results.html",{'result': quiz_results, 'bestResults': best_results})
    else:
        categoryQuizz = 17
        nbQuestions = 4
        difficultyQuestions = 'easy'
        triviaURL = f'https://opentdb.com/api.php?amount={nbQuestions}&category={categoryQuizz}&difficulty={difficultyQuestions}&type=multiple'
        triviaResponse = requests.get(triviaURL)
        questions = triviaResponse.json()['results']
        for question in questions:
                question['id'] = questions.index(question) + 1
        request.session['questions'] = questions
        return render(request, "quizz/quizz.html",  {'questions': questions})


