from django.test import SimpleTestCase
from django.urls import reverse, resolve
from .views import mon_cv

class HomepageTests(SimpleTestCase):
    def setUp(self): 
        url = reverse("home")
        self.response = self.client.get(url)
    def test_url_exists_at_correct_location(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


    def test_homepage_template(self): 
        response = self.client.get("/")
        self.assertTemplateUsed(response, "pages/home.html")



    def test_homepage_url_resolves_homepageview(self): # new
        view = resolve("/")
        self.assertEqual(view.func.__name__, mon_cv.__name__)
