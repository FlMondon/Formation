# pages/views.py


from django.shortcuts import render

def mon_cv(request):
    # Informations personnelles
    nom = "MONDON"
    prenom = "Florian"
    telephone = "0668797223"
    email = "florian.mondon@orange.fr"

    # Formations
    formations = [
        {
            "titre": "Doctorat en Physique, spécialité cosmologie",
            "date_debut": "2017",
            "date_fin": "2020",
            "etablissement": "Université Clermont Auvergne",
            "ville": "Clermont-Ferrand, France"
        },
        {
            "titre": "Master en Physique, spécialité astrophysique, cosmologie et physique des particules",
            "date_debut": "2015",
            "date_fin": "2017",
            "etablissement": "Université de Montpellier",
            "ville": "Montpellier"
        }
    ]

    # Expérience professionnelle
    experiences = [
        {
            "poste": "Data scientist",
            "date_debut": "2017",
            "date_fin": "2020",
            "entreprise": "LPC - IN2P3/CNRS",
            "ville": "Clermont-Ferrand",
            "taches": [
                "Simulation et modélisation de données de série temporelles",
                "Évaluation des performances de modélisation de série temporelles",
                "Détection d'anonalie sur des données astrophysique basé sur du Machine learning"
            ]
        },
        {
            "poste": "Data scientist",
            "date_debut": "2021",
            "date_fin": "En cours",
            "entreprise": "SII",
            "ville": "Paris",
            "taches": [
                "Création d'un système de détection de Cyber Attaque par Intelligence Artificielle",
                "Simulation d'un jeu de données IT dans le cadre de la Cyber sécurité",
            ]
        }
    ]

    # Compétences
    competences = [
        "Méthode statistique pour l analyse de données: Régression linéaire, Analyse en composante principale, Restricted Maximum Likelihood",
        "Expert en librairie Python: NumPy, iminuit, Scikit-learn, Tesnsorflow, Pandas",
        "Machine Learning pour la détection d'anomalie: Isolation forest, Active Learning",
    ]

    context = {
        "nom": nom,
        "prenom": prenom,
        "telephone": telephone,
        "email": email,
        "formations": formations,
        "experiences": experiences,
        "competences": competences
    }

    return render(request, "pages/home.html", context)


